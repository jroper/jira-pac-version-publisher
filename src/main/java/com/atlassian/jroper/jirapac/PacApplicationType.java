package com.atlassian.jroper.jirapac;

import com.atlassian.applinks.spi.application.StaticUrlApplicationType;
import com.atlassian.applinks.spi.application.TypeId;

import java.net.URI;

/**
 * Application type with a static URL
 */
public class PacApplicationType implements StaticUrlApplicationType {
    public static final TypeId TYPE_ID = new TypeId("pac");
    @Override
    public URI getStaticUrl() {
        return URI.create("https://plugins.atlassian.com");
    }

    @Override
    public String getI18nKey() {
        return "Atlassian Plugin Exchange";
    }

    @Override
    public URI getIconUrl() {
        return URI.create("https://plugins.atlassian.com/s/9/images/favicon.ico");
    }

    @Override
    public TypeId getId() {
        return TYPE_ID;
    }


}
