package com.atlassian.jroper.jirapac;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.BasicAuthenticationProvider;
import com.atlassian.applinks.spi.Manifest;
import com.atlassian.applinks.spi.application.ApplicationIdUtil;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.applinks.spi.manifest.ApplicationStatus;
import com.atlassian.applinks.spi.manifest.ManifestNotFoundException;
import com.atlassian.applinks.spi.manifest.ManifestProducer;

import java.net.URI;
import java.util.Collections;
import java.util.Set;

/**
 * Manifest producer
 */
public class PacManifestProducer implements ManifestProducer {
    public static final ApplicationId applicationId = ApplicationIdUtil.generate(URI.create("https://plugins.atlassian.com"));
    @Override
    public Manifest getManifest(URI uri) throws ManifestNotFoundException {
        return new Manifest() {
            @Override
            public ApplicationId getId() {
                return applicationId;
            }

            @Override
            public String getName() {
                return "Atlassian Plugin Exchange";
            }

            @Override
            public TypeId getTypeId() {
                return PacApplicationType.TYPE_ID;
            }

            @Override
            public String getVersion() {
                return null;
            }

            @Override
            public Long getBuildNumber() {
                return 0L;
            }

            @Override
            public URI getUrl() {
                return URI.create("https://plugins.atlassian.com");
            }

            @Override
            public org.osgi.framework.Version getAppLinksVersion() {
                return null;
            }

            @Override
            public Set<Class<? extends AuthenticationProvider>> getInboundAuthenticationTypes() {
                return Collections.<Class<? extends AuthenticationProvider>>singleton(BasicAuthenticationProvider.class);
            }

            @Override
            public Set<Class<? extends AuthenticationProvider>> getOutboundAuthenticationTypes() {
                return Collections.emptySet();
            }

            @Override
            public Boolean hasPublicSignup() {
                return true;
            }
        };
    }

    @Override
    public ApplicationStatus getStatus(URI uri) {
        return ApplicationStatus.AVAILABLE;
    }
}
