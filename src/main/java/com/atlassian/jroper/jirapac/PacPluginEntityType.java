package com.atlassian.jroper.jirapac;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.spi.application.NonAppLinksEntityType;
import com.atlassian.applinks.spi.application.TypeId;

import java.net.URI;

/**
 * A plugin on PAC
 */
public class PacPluginEntityType implements NonAppLinksEntityType {

    public static final TypeId TYPE_ID = new TypeId("pacPlugin");

    @Override
    public TypeId getId() {
        return TYPE_ID;
    }

    @Override
    public Class<? extends ApplicationType> getApplicationType() {
        return PacApplicationType.class;
    }

    @Override
    public String getI18nKey() {
        return "Plugin";
    }

    @Override
    public String getPluralizedI18nKey() {
        return "Plugins";
    }

    @Override
    public String getShortenedI18nKey() {
        return "Plugin";
    }

    @Override
    public URI getIconUrl() {
        return URI.create("https://plugins.atlassian.com/s/9/images/favicon.ico");
    }

    @Override
    public URI getDisplayUrl(ApplicationLink link, String entityKey) {
        return URI.create("https://plugins.atlassian.com/plugins/" + entityKey);
    }
}
