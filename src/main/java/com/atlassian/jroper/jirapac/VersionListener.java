package com.atlassian.jroper.jirapac;

import com.atlassian.applinks.api.*;
import com.atlassian.applinks.api.auth.types.BasicAuthenticationProvider;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.project.VersionReleaseEvent;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.*;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class VersionListener implements DisposableBean
{
    private static final Logger log = LoggerFactory.getLogger(VersionListener.class);
    private final EventPublisher eventPublisher;
    private final EntityLinkService entityLinkService;
    private final VersionManager versionManager;
    private final AuthenticationConfigurationManager authenticationConfigurationManager;
    private final WebClient webClient;
    private final ApplicationProperties applicationProperties;
    private final ExecutorService executorService;

    public VersionListener(EventPublisher eventPublisher, EntityLinkService entityLinkService, VersionManager versionManager, AuthenticationConfigurationManager authenticationConfigurationManager, ApplicationProperties applicationProperties) {
        this.entityLinkService = entityLinkService;
        this.versionManager = versionManager;
        this.authenticationConfigurationManager = authenticationConfigurationManager;
        this.applicationProperties = applicationProperties;
        eventPublisher.register(this);
        this.eventPublisher = eventPublisher;
        this.webClient = new WebClient();
        webClient.setJavaScriptEnabled(false);
        webClient.setCssEnabled(false);
        this.executorService = Executors.newSingleThreadExecutor();
    }

    @Override
    public void destroy() throws Exception {
        eventPublisher.unregister(this);
        webClient.closeAllWindows();
        executorService.shutdown();
    }

    @EventListener
    public void onVersionReleased(VersionReleaseEvent event) throws Exception {
        final Version version = versionManager.getVersion(event.getVersionId());
        final EntityLink entityLink = entityLinkService.getPrimaryEntityLink(version.getProjectObject(), PacPluginEntityType.class);
        final Map<String, String> config = authenticationConfigurationManager.getConfiguration(entityLink.getApplicationLink().getId(), BasicAuthenticationProvider.class);
        final String username = config.get("username");
        final String password = config.get("password");

        // Do the rest in the background
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    createNewPacRelease(version, entityLink, username, password);
                } catch (IOException e) {
                    log.error("Error creating release", e);
                }
            }
        });
    }

    private void createNewPacRelease(Version version, EntityLink entityLink, String username, String password) throws IOException {
        log.warn("Going to login");
        // First login
        HtmlPage page = webClient.getPage("https://plugins.atlassian.com/login");
        log.warn("Got login page: " + page.getTitleText());
        HtmlForm form = (HtmlForm) page.getElementById("loginform");
        form.getInputByName("username").setValueAttribute(username);
        form.getInputByName("password").setValueAttribute(password);
        page = form.getElementsByAttribute("input", "type", "submit").get(0).click();
        log.warn("Result of login: " + page.getTitleText());
        // Hopefully we're logged in now.  Go to the create version screen
        page = webClient.getPage("https://plugins.atlassian.com/manage/plugins/" + entityLink.getKey() + "/versions/create");
        log.warn("Got create version page: " + page.getTitleText());
        savePage(page, "create.html");
        // Set the version name
        ((HtmlInput) page.getElementById("version")).setValueAttribute(version.getName());
        ((HtmlInput) page.getElementById("releaseSummary")).setValueAttribute(version.getDescription());
        ((HtmlInput) page.getElementById("releaseNotesUrl")).setValueAttribute(
                applicationProperties.getBaseUrl() + "/browse/" + version.getProjectObject().getKey() + "/fixforversion/" + version.getId());
        // Submit the form
        // I don't know why this is so hard...
        for (HtmlElement input : page.getElementsByTagName("input")) {
            if (input.getAttribute("type").equals("submit") && input.getAttribute("class").equals("save")) {
                input.click();
                break;
            }
        }
        log.warn("Submitted create version: " + page.getTitleText());
        savePage(page, "saved.html");
        // Finally log out
        webClient.getPage("https://plugins.atlassian.com/logout");
    }

    private void savePage(HtmlPage page, String file) {
        OutputStream os = null;
        try {
            os = new FileOutputStream("/tmp/" + file);
            os.write(page.asXml().getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            IOUtils.closeQuietly(os);
        }

    }
}
